// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

function defaults(obj = {}, defaultProps = {}) {
  if (
    obj.constructor !== Object ||
    defaultProps.constructor !== Object ||
    !obj ||
    !defaultProps
  ) {
    return {};
  } else {
    for (let keys in defaultProps) {
      if (obj[keys] === undefined) {
        obj[keys] = defaultProps[keys];
      } else {
        continue;
      }
    }
    return obj;
  }
}

module.exports = defaults;


