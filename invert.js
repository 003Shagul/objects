// Returns a copy of the object where the keys have become the values and the values the keys.
// Assume that all of the object's values will be unique and string serializable.
// http://underscorejs.org/#invert

function invert(obj = {}) {
  if (obj.constructor !== Object || !obj) {
    return {};
  } else {
    let newObj = {};
    for (let keys in obj) {
      let val = obj[keys];
      newObj[val] = keys;
    }
    return newObj;
  }
}

module.exports = invert;


