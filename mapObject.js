// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject

function mapObject(obj = {}, cb) {
  if (obj.constructor !== Object || !obj || !cb) {
    return {};
  } else {
    let newObj = {};
    for (let keys in obj) {
      let newVal = cb(obj[keys], keys);
      newObj[keys] = newVal;
    }
    return newObj;
  }
}

module.exports = mapObject;


