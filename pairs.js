// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs

function pairs(obj = {}) {
  if (obj.constructor !== Object || !obj) {
    return [];
  } else {
    let newArr = [];
    for (let keys in obj) {
      let newVal = [keys, obj[keys]];
      newArr.push(newVal);
    }
    return newArr;
  }
}

module.exports = pairs;

