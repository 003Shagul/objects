function values(obj = {}) {
  if (!obj.constructor === Object || !obj) {
    return [];
  }
  let array = [];
  for (let keys in obj) {
    if (typeof obj[keys] === "function") {
      continue;
    } else {
      array.push(obj[keys]);
    }
  }
  return array;
}

module.exports = values;


